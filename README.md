# face-api.js 使用模型资源文件

## 简介

本仓库提供了一个用于 `face-api.js` 的模型资源文件。`face-api.js` 是一个基于 TensorFlow.js 的 JavaScript API，专门用于在浏览器中进行人脸检测、人脸识别和人脸特征点检测。

## 资源文件说明

本仓库中的资源文件是 `face-api.js` 所需的关键模型文件。这些模型文件是 `face-api.js` 在执行人脸检测、人脸识别和人脸特征点检测时所必需的。

## 使用方法

1. **下载模型文件**：
   - 你可以通过克隆本仓库或直接下载压缩包的方式获取模型文件。

2. **配置 `face-api.js`**：
   - 在你的项目中引入 `face-api.js`，并确保模型文件的路径正确配置。

   ```javascript
   const MODEL_URL = '/path/to/model/files';
   await faceapi.loadSsdMobilenetv1Model(MODEL_URL);
   await faceapi.loadFaceLandmarkModel(MODEL_URL);
   await faceapi.loadFaceRecognitionModel(MODEL_URL);
   ```

3. **使用 `face-api.js` 进行人脸检测**：
   - 你可以使用 `face-api.js` 提供的 API 进行人脸检测、识别和特征点检测。

   ```javascript
   const detections = await faceapi.detectAllFaces(input)
                                 .withFaceLandmarks()
                                 .withFaceDescriptors();
   ```

## 模型文件列表

- `ssd_mobilenetv1_model.json`
- `face_landmark_68_model.json`
- `face_recognition_model.json`

## 贡献

如果你在使用过程中发现任何问题或有改进建议，欢迎提交 Issue 或 Pull Request。

## 许可证

本仓库中的资源文件遵循 MIT 许可证。详细信息请参阅 [LICENSE](LICENSE) 文件。

---

希望这个资源文件能帮助你在 `face-api.js` 项目中顺利进行人脸检测和识别！